# venv

## Windows cmd.exe

1. Display python version.
```shell
$ py --version
Python 3.9.6
```

2. Make virtual environment to venv directory.
```shell
$ py -m venv venv
```

3. Activate virual envionment.
```shell
$ venv\Scripts\activate.bat
(venv) $ _
```

4. Update pip and install by list.
```shell
(venv) $ py -m pip install --upgrade pip
  :
(venv) $ pip list
Package    Version
---------- -------
pip        21.2.2
setuptools 56.0.0
(venv) $ py -m pip install -r requirements.txt
  :
```

## Ubuntu 18.04LTS
1. Display python version.
```shell
$ python3 --version
Python 3.6.9
```

2. Make virtual environment to .venv directory.
```shell
$ python3 -m venv .venv
```

3. Activate virual envionment.
```shell
$ .venv\Scripts\activate.bat
(.venv) $ _
```

4. Update pip and install by list.
```shell
(.venv) $ py -m pip install --upgrade pip
  :
(.venv) $ pip list
Package    Version
---------- -------
pip        21.2.2
setuptools 56.0.0
$ py -m pip install -r requirements.txt
```
